import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumeMissionComponent } from './resume-mission.component';

describe('ResumeMissionComponent', () => {
  let component: ResumeMissionComponent;
  let fixture: ComponentFixture<ResumeMissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumeMissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumeMissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
