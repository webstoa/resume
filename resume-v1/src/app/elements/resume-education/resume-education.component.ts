import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { ResumeViewDataService } from '../../services/resume-view-data.service';


@Component({
  selector: 'ws-resume-education',
  templateUrl: './resume-education.component.html',
  styleUrls: ['./resume-education.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ResumeEducationComponent implements OnInit {

  constructor(private viewDataService: ResumeViewDataService) {
  }

  ngOnInit() {
  }

}
