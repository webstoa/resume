import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { ResumeViewDataService } from '../../services/resume-view-data.service';


@Component({
  selector: 'ws-resume-synopsis',
  templateUrl: './resume-synopsis.component.html',
  styleUrls: ['./resume-synopsis.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ResumeSynopsisComponent implements OnInit {

  constructor(private viewDataService: ResumeViewDataService) {
  }

  ngOnInit() {
  }

}
