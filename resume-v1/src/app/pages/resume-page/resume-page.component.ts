import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ws-resume-page',
  templateUrl: './resume-page.component.html',
  styleUrls: ['./resume-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ResumePageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
